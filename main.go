package main

import (
	"fmt"
	"os"

	"gitee.com/zhaobingss/unipdf/v3/extractor"
	"gitee.com/zhaobingss/unipdf/v3/model"
)

func main() {
	//os.Setenv("UNIDOC_LICENSE_SERVER_URL", "http://localhost:9988")
	//err := license.SetMeteredKey("123")
	//if err != nil {
	//	panic(err)
	//}

	inPath := "BSP.PDF"
	err := outputPdfText(inPath)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}

}

func outputPdfText(inputPath string) error {
	f, err := os.Open(inputPath)
	if err != nil {
		return err
	}

	defer f.Close()

	pdfReader, err := model.NewPdfReader(f)
	if err != nil {
		return err
	}

	numPages, err := pdfReader.GetNumPages()
	if err != nil {
		return err
	}

	fmt.Printf("--------------------\n")
	fmt.Printf("PDF to text extraction:\n")
	fmt.Printf("--------------------\n")
	for i := 0; i < numPages; i++ {

		if i == 0 {
			continue
		}

		pageNum := i + 1

		page, err := pdfReader.GetPage(pageNum)
		if err != nil {
			return err
		}

		ex, err := extractor.New(page)
		if err != nil {
			return err
		}

		text, err := ex.ExtractText()
		if err != nil {
			return err
		}

		pt, i, j, err := ex.ExtractPageText()
		fmt.Println(pt, i, j, err)

		markArr := pt.Marks()

		fmt.Println(markArr)

		marks := markArr.Elements()

		fmt.Println(marks)

		fmt.Println("------------------------------")
		fmt.Printf("Page %d:\n", pageNum)
		fmt.Printf("\"%s\"\n", text)
		fmt.Println("------------------------------")

		if pageNum == 2 {
			break
		}
	}

	return nil
}
